package Test;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.*;
import static io.restassured.RestAssured.*;
public class SimpleBooksTesting {
    private static String accessToken;
    private static String customerName;
    private static String orderId;
    @Test (priority =1 )
    public void test1(){
        baseURI = "https://simple-books-api.glitch.me";
        given()
                .get("/books/4")
                .then()
                .statusCode(200);
    }

    @Test(priority = 2)
    public void testPostAPIAuthentication() {
        baseURI = "https://simple-books-api.glitch.me";
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        for (int index = 0; index < 4; index++) {
            customerName += characters.charAt((int) Math.floor(Math.random() * characters.length()));
        }
        JSONObject requestTokenBody = new JSONObject();
        requestTokenBody.put("clientName", customerName);
        requestTokenBody.put("clientEmail", customerName + "@gmail.com");
        accessToken = given()
                .header("Content-Type", "application/json")
                .body(requestTokenBody.toJSONString())
                .when()
                .post("/api-clients")
                .then()
                .statusCode(201)
                .extract()
                .path("accessToken");
    }

    @Test(priority = 3)
    public void testPostSubmitOrder() {
        baseURI = "https://simple-books-api.glitch.me";
        JSONObject requestOrder = new JSONObject();
        requestOrder.put("bookId", 6);
        requestOrder.put("customerName", customerName);
        orderId = given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + accessToken)
                .body(requestOrder.toJSONString())
                .when()
                .post("/orders")
                .then()
                .statusCode(201)
                .body("created", equalTo(true))
                .extract()
                .path("orderId");
    }

    @Test(priority = 4)
    public void testPatchOrder() {
        baseURI = "https://simple-books-api.glitch.me";
        JSONObject request = new JSONObject();
        request.put("customerName", "Arun");
        given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + accessToken)
                .body(request.toJSONString())
                .when()
                .patch("/orders/" + orderId)
                .then()
                .statusCode(204);
    }

    @Test(priority = 5)
    public void testDeleteOrder() {
        baseURI = "https://simple-books-api.glitch.me";
        JSONObject request = new JSONObject();
        request.put("customerName", "Arun");
        given()
                .header("Content-Type", "application/json")
                .header("Authorization","Bearer "+ accessToken)
                .body(request.toJSONString())
                .when()
                .delete("orders/"+orderId)
                .then()
                .statusCode(204);
    }
}
